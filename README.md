# A1200BifiLED

This project is a variant of my earlier A1200HDDLED. I recently got a nice Bifrost LED module and desired a convenient one-plug solution for the connector to the Bifrost. Contrary to A1200HDDLED which always merges a second LOW active input to the HDD LED, this project can utilize the CH1 and CH2 auxiliary channels of the Bifrost.

The second revision of this adapter allows more flexible use. While it can serve the functionality to add more light sources to a BiFrost, it also can light up the HDD LED on a regular original A1200 LED module.



## Pictures

The picture below shows Revision1 of the board. 
![installed in A1200](https://gitlab.com/HenrykRichter/a1200hddled/raw/master/Pics/ideled_a1200.jpg)

The second picture contains a rendering of the revision 2 in this repository. Depending on application, a pin header might be more convenient than the Dupont cable I've soldered directly to my prototype board.
![rendering of Rev2](https://gitlab.com/HenrykRichter/a1200hddled/raw/master/Pics/BifiLED2_Front.png)
![rendering of Rev2](https://gitlab.com/HenrykRichter/a1200hddled/raw/master/Pics/BifiLED2_Back.png)

## Parts list
- 2 PNP Transistors SOT23 (e.g. MMBT3906, BC807 or similar)
- 1 74HC03D (SOIC)
- 4 Resistors 4.7k 0805
- 2 Resistors 10k 0805
- 2 Resistors 15k 0805 (10k is also suitable)
- 2 Capacitors 100nF-1uF 0805 (optional)
- 1 Pin Strip RM2.54 (0.1") 8 positions
- 1 Socket Strip RM2.54 (0.1") 5 positions
- 1 Pin Strip RM2.54 (0.1") 3 positions
- 1 Dupont plug with cable, 2 Pins

## Building and installation notes
Soldering should be straightforward for the SMD components. The pin strip J2 is also populated on the component side. Please remove pin 4 before soldering J2. The original A1200 LED modules usually have a key pin there. The socket strip J3 needs to be inserted from the bottom of the board to mate with the connector in the A1200.

The PCB fits only in one orientation on the A1200 board, i.e. such that the SMD components are pointing left.

The 5V pin of J1 needs to be connected to a 5V source. The Vampire1200 also has a convenient 5V output available nearby by means of the HDD+ pin. Please note that the HDD+ pin of the V1200 does not provide sufficient power for the BiFrost Heimdall Edition extended mode. If you desire to use it, then you might consider the Floppy connector as a source for +5V.

If the module shall be used with an original A1200 LED module, then the first input (CH1) can be mapped onto the classic HDD indicator. Please close the ORI_LED1 solder jumper on the bottom of the PCB in that case.

The PCMCIA signal (/CC_ENA) can be obtained from the left pad of either R637 or R638, if desired.


## Special use cases

Recently, a number of IDE devices appeared on the market which lead to constantly lit up HDD LEDs in Amiga Computers (SD2IDE44, some CF cards) due to their 3.3V design. The auxiliary channels on this module are prepared for that case and contain a CMOS based level shifter. Since the regular A1200 circuit is located in front of the LED connector, devices connected to the Mainboard IDE will still show the incorrect behaviour. 

A possible solution for such cases is to connect a wire to Pin39 of the Mainboard connector (or the device itself) and hook it up to In2 of this module. Then replace the LED module Plug by a 6 pin Dupont plug and reconnect the wires such that the third pin from the left (HDD) goes to CH2. The other pin locations can remain in their original state.

