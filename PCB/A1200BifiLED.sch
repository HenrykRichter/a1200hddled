EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "A1200BifiLED"
Date ""
Rev "0.4"
Comp "Henryk Richter"
Comment1 "and more Power."
Comment2 "the BiFrost LED module with additional inputs"
Comment3 "Adapter for the Amiga1200 LED header to support"
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x05 J3
U 1 1 5E219BF8
P 1350 1050
F 0 "J3" H 1430 1092 50  0000 L CNN
F 1 "Socket_IN" H 1430 1001 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 1350 1050 50  0001 C CNN
F 3 "~" H 1350 1050 50  0001 C CNN
	1    1350 1050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5E21A735
P 1350 1950
F 0 "J2" H 1430 1992 50  0000 L CNN
F 1 "Header_OUT" H 1430 1901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 1350 1950 50  0001 C CNN
F 3 "~" H 1350 1950 50  0001 C CNN
	1    1350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 850  900  850 
Text Label 1100 850  2    50   ~ 0
PLED
Wire Wire Line
	1150 950  900  950 
Wire Wire Line
	1150 1050 900  1050
Wire Wire Line
	1150 1250 900  1250
Wire Wire Line
	1150 1950 950  1950
Wire Wire Line
	1150 2050 950  2050
Wire Wire Line
	1150 2150 950  2150
Wire Wire Line
	1150 2350 950  2350
$Comp
L power:GND #PWR0101
U 1 1 5E21C154
P 900 1250
F 0 "#PWR0101" H 900 1000 50  0001 C CNN
F 1 "GND" H 905 1077 50  0000 C CNN
F 2 "" H 900 1250 50  0001 C CNN
F 3 "" H 900 1250 50  0001 C CNN
	1    900  1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E21C596
P 950 2350
F 0 "#PWR0102" H 950 2100 50  0001 C CNN
F 1 "GND" H 955 2177 50  0000 C CNN
F 2 "" H 950 2350 50  0001 C CNN
F 3 "" H 950 2350 50  0001 C CNN
	1    950  2350
	1    0    0    -1  
$EndComp
Text Label 1150 950  2    50   ~ 0
FloppyLED
Text Label 1150 1050 2    50   ~ 0
HDDLED_IN
Text Label 1150 1950 2    50   ~ 0
PLED
Text Label 1150 2050 2    50   ~ 0
FloppyLED
Text Label 1150 2150 2    50   ~ 0
HDDLED
$Comp
L Device:R R2
U 1 1 5E21E9AD
P 1600 3500
F 0 "R2" V 1393 3500 50  0000 C CNN
F 1 "4.7k" V 1484 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1530 3500 50  0001 C CNN
F 3 "~" H 1600 3500 50  0001 C CNN
	1    1600 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5E21F535
P 1300 3350
F 0 "R1" H 1230 3304 50  0000 R CNN
F 1 "4.7k" H 1230 3395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1230 3350 50  0001 C CNN
F 3 "~" H 1300 3350 50  0001 C CNN
	1    1300 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 3500 1300 3500
Connection ~ 1300 3500
Wire Wire Line
	1300 3500 1100 3500
Wire Wire Line
	1300 3200 1100 3200
Wire Wire Line
	1300 3200 2050 3200
Wire Wire Line
	2050 3200 2050 3300
Connection ~ 1300 3200
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5E222383
P 3850 1100
F 0 "J1" H 3768 775 50  0000 C CNN
F 1 "HDDLEDIn" H 3768 866 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 3850 1100 50  0001 C CNN
F 3 "~" H 3850 1100 50  0001 C CNN
	1    3850 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1100 3200 1100 3300
Wire Wire Line
	1100 3300 950  3300
Wire Wire Line
	1100 3500 1100 3400
Wire Wire Line
	1100 3400 950  3400
$Comp
L Device:C C1
U 1 1 5E21C74A
P 1300 3650
F 0 "C1" H 1415 3696 50  0000 L CNN
F 1 "1uF" H 1415 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1338 3500 50  0001 C CNN
F 3 "~" H 1300 3650 50  0001 C CNN
	1    1300 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5E21CDC2
P 1300 3800
F 0 "#PWR0103" H 1300 3550 50  0001 C CNN
F 1 "GND" H 1305 3627 50  0000 C CNN
F 2 "" H 1300 3800 50  0001 C CNN
F 3 "" H 1300 3800 50  0001 C CNN
	1    1300 3800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:MMBT3906 Q1
U 1 1 5E4EB174
P 1950 3500
F 0 "Q1" H 2141 3454 50  0000 L CNN
F 1 "MMBT3906" H 2141 3545 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2150 3425 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3906.pdf" H 1950 3500 50  0001 L CNN
	1    1950 3500
	1    0    0    1   
$EndComp
Text Label 750  2250 0    50   ~ 0
PCMCIALED
Wire Wire Line
	750  2250 1150 2250
Wire Wire Line
	725  1150 1150 1150
Text Label 725  1150 0    50   ~ 0
PCMCIALED
$Comp
L Device:R R4
U 1 1 5F5412A0
P 1750 6350
F 0 "R4" V 1543 6350 50  0000 C CNN
F 1 "4.7k" V 1634 6350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1680 6350 50  0001 C CNN
F 3 "~" H 1750 6350 50  0001 C CNN
	1    1750 6350
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5F5412A6
P 1450 6200
F 0 "R3" H 1380 6154 50  0000 R CNN
F 1 "4.7k" H 1380 6245 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1380 6200 50  0001 C CNN
F 3 "~" H 1450 6200 50  0001 C CNN
	1    1450 6200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 6350 1450 6350
Connection ~ 1450 6350
Wire Wire Line
	1450 6350 1250 6350
Wire Wire Line
	1450 6050 1250 6050
Wire Wire Line
	1450 6050 2200 6050
Wire Wire Line
	2200 6050 2200 6150
Connection ~ 1450 6050
Wire Wire Line
	1250 6050 1250 6150
Wire Wire Line
	1250 6150 1100 6150
Wire Wire Line
	1250 6350 1250 6250
Wire Wire Line
	1250 6250 1100 6250
$Comp
L Device:C C2
U 1 1 5F5412B9
P 1450 6500
F 0 "C2" H 1565 6546 50  0000 L CNN
F 1 "1uF" H 1565 6455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1488 6350 50  0001 C CNN
F 3 "~" H 1450 6500 50  0001 C CNN
	1    1450 6500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F5412BF
P 1450 6650
F 0 "#PWR0104" H 1450 6400 50  0001 C CNN
F 1 "GND" H 1455 6477 50  0000 C CNN
F 2 "" H 1450 6650 50  0001 C CNN
F 3 "" H 1450 6650 50  0001 C CNN
	1    1450 6650
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:MMBT3906 Q2
U 1 1 5F5412C5
P 2100 6350
F 0 "Q2" H 2291 6304 50  0000 L CNN
F 1 "MMBT3906" H 2291 6395 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2300 6275 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3906.pdf" H 2100 6350 50  0001 L CNN
	1    2100 6350
	1    0    0    1   
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5F54213F
P 4050 1000
F 0 "#PWR0105" H 4050 850 50  0001 C CNN
F 1 "+5V" H 4065 1173 50  0000 C CNN
F 2 "" H 4050 1000 50  0001 C CNN
F 3 "" H 4050 1000 50  0001 C CNN
	1    4050 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5F542834
P 950 3300
F 0 "#PWR0106" H 950 3150 50  0001 C CNN
F 1 "+5V" H 965 3473 50  0000 C CNN
F 2 "" H 950 3300 50  0001 C CNN
F 3 "" H 950 3300 50  0001 C CNN
	1    950  3300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5F542DF8
P 1100 6150
F 0 "#PWR0107" H 1100 6000 50  0001 C CNN
F 1 "+5V" H 1115 6323 50  0000 C CNN
F 2 "" H 1100 6150 50  0001 C CNN
F 3 "" H 1100 6150 50  0001 C CNN
	1    1100 6150
	1    0    0    -1  
$EndComp
Text GLabel 2400 3800 2    50   Output ~ 0
CH1
Text GLabel 2550 6650 2    50   Output ~ 0
CH2
Wire Wire Line
	2050 3700 2050 3800
Wire Wire Line
	2050 3800 2400 3800
Wire Wire Line
	2200 6550 2200 6650
Wire Wire Line
	2200 6650 2550 6650
Text GLabel 4150 1100 2    50   Input ~ 0
inCH1
Text GLabel 4150 1200 2    50   Input ~ 0
inCH2
Text GLabel 4000 3500 0    50   Input ~ 0
inCH1
Text GLabel 4000 5150 0    50   Input ~ 0
inCH2
Wire Wire Line
	4050 1100 4150 1100
Wire Wire Line
	4050 1200 4150 1200
$Comp
L power:+5V #PWR0108
U 1 1 5F549AF2
P 1150 1650
F 0 "#PWR0108" H 1150 1500 50  0001 C CNN
F 1 "+5V" H 1165 1823 50  0000 C CNN
F 2 "" H 1150 1650 50  0001 C CNN
F 3 "" H 1150 1650 50  0001 C CNN
	1    1150 1650
	1    0    0    -1  
$EndComp
Text GLabel 900  1750 0    50   Input ~ 0
CH1
Text GLabel 900  1850 0    50   Input ~ 0
CH2
Wire Wire Line
	900  1750 1150 1750
Wire Wire Line
	900  1850 1150 1850
$Comp
L Jumper:SolderJumper_2_Open Ori_LED1
U 1 1 6036670A
P 1800 5250
F 0 "Ori_LED1" H 1800 5455 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1800 5364 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1800 5250 50  0001 C CNN
F 3 "~" H 1800 5250 50  0001 C CNN
	1    1800 5250
	1    0    0    -1  
$EndComp
Text GLabel 1650 5250 0    50   Input ~ 0
CH1
Wire Wire Line
	1950 5250 2300 5250
Text Label 2250 5250 2    50   ~ 0
HDDLED
$Comp
L 74xx:74LS03 U1
U 1 1 6037B2CA
P 4700 3500
F 0 "U1" H 4700 3825 50  0000 C CNN
F 1 "74HC03" H 4700 3734 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4700 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS03" H 4700 3500 50  0001 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS03 U1
U 2 1 6037DBDD
P 4700 4050
F 0 "U1" H 4700 4375 50  0000 C CNN
F 1 "74HC03" H 4700 4284 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4700 4050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS03" H 4700 4050 50  0001 C CNN
	2    4700 4050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS03 U1
U 3 1 60381757
P 4700 4600
F 0 "U1" H 4700 4925 50  0000 C CNN
F 1 "74HC03" H 4700 4834 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4700 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS03" H 4700 4600 50  0001 C CNN
	3    4700 4600
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS03 U1
U 4 1 603844F0
P 4700 5150
F 0 "U1" H 4700 5475 50  0000 C CNN
F 1 "74HC03" H 4700 5384 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4700 5150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS03" H 4700 5150 50  0001 C CNN
	4    4700 5150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS03 U1
U 5 1 6038793E
P 4700 6150
F 0 "U1" H 4930 6196 50  0000 L CNN
F 1 "74HC03" H 4930 6105 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4700 6150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS03" H 4700 6150 50  0001 C CNN
	5    4700 6150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 6039F3CD
P 4700 5650
F 0 "#PWR0109" H 4700 5500 50  0001 C CNN
F 1 "+5V" H 4715 5823 50  0000 C CNN
F 2 "" H 4700 5650 50  0001 C CNN
F 3 "" H 4700 5650 50  0001 C CNN
	1    4700 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 6039FA76
P 4700 6650
F 0 "#PWR0110" H 4700 6400 50  0001 C CNN
F 1 "GND" H 4705 6477 50  0000 C CNN
F 2 "" H 4700 6650 50  0001 C CNN
F 3 "" H 4700 6650 50  0001 C CNN
	1    4700 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3400 4400 3500
Wire Wire Line
	4400 3950 4400 4050
Wire Wire Line
	4400 4500 4400 4600
Wire Wire Line
	4400 5050 4400 5150
Connection ~ 4400 3500
Wire Wire Line
	4400 3500 4400 3600
Wire Wire Line
	5000 3500 5000 3800
Wire Wire Line
	5000 3800 4300 3800
Wire Wire Line
	4300 3800 4300 4050
Wire Wire Line
	4300 4050 4400 4050
Connection ~ 4400 4050
Wire Wire Line
	4400 4050 4400 4150
Text GLabel 950  3400 0    50   Input ~ 0
BCh1
Text GLabel 5100 4050 2    50   Input ~ 0
BCh1
Wire Wire Line
	5000 4050 5100 4050
Connection ~ 4400 5150
Wire Wire Line
	4400 5150 4400 5250
Wire Wire Line
	5000 5150 5000 4850
Wire Wire Line
	5000 4850 4300 4850
Wire Wire Line
	4300 4850 4300 4600
Wire Wire Line
	4300 4600 4400 4600
Connection ~ 4400 4600
Wire Wire Line
	4400 4600 4400 4700
Text GLabel 5100 4600 2    50   Input ~ 0
BCh2
Text GLabel 1100 6250 0    50   Input ~ 0
BCh2
Wire Wire Line
	5000 4600 5100 4600
$Comp
L Jumper:SolderJumper_2_Open NoU1.1
U 1 1 603C999F
P 5950 4050
F 0 "NoU1.1" H 5950 4255 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 5950 4164 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 5950 4050 50  0001 C CNN
F 3 "~" H 5950 4050 50  0001 C CNN
	1    5950 4050
	1    0    0    -1  
$EndComp
Text GLabel 5800 4050 0    50   Input ~ 0
inCH1
Text GLabel 6100 4050 2    50   Input ~ 0
BCh1
$Comp
L Jumper:SolderJumper_2_Open NoU1.2
U 1 1 603CE20D
P 5950 4600
F 0 "NoU1.2" H 5950 4805 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 5950 4714 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 5950 4600 50  0001 C CNN
F 3 "~" H 5950 4600 50  0001 C CNN
	1    5950 4600
	1    0    0    -1  
$EndComp
Text GLabel 5800 4600 0    50   Input ~ 0
inCH2
Text GLabel 6100 4600 2    50   Input ~ 0
BCh2
Text Notes 3700 3050 0    50   ~ 0
U1 is optional for 3.3V based inputs (especially with 3.3V pull-ups)\nand works as level shifter here. This method applies to some SD-IDE adapters.\n\nIf U1/R5/R6 are not populated, then just close the two SJs.
Text Notes 700  5850 0    50   ~ 0
R3, R4 and Q2 form an optional second input for use with the new \nBiFrost Heimdall Edition LED assembly. If a second indicator is not \nrelevant, then these can be omitted.
Wire Notes Line
	6800 2650 6800 6900
Wire Notes Line
	6800 6900 3650 6900
Wire Notes Line
	3650 6900 3650 2650
Text Notes 700  3050 0    50   ~ 0
This is the main circuit. It will take low-active inputs and is able\nto drive high-active LEDs (like those in A500 and A1200). Please\nnote that this circuit will form a logic OR with the existing LED driving\ncircuit.
Wire Notes Line
	650  2650 3550 2650
Wire Notes Line
	3550 2650 3550 4050
Wire Notes Line
	3550 4050 650  4050
Wire Notes Line
	650  4050 650  2650
Text Notes 700  4800 0    50   ~ 0
This module is supposed to be compatible with the factory stock\nA1200 LED module as well as the new BiFrost Heimdall Edition.\n\nThe SolderJumper Ori_LED1 is needs to be closed when the\nstock A1200 LED module is in use. It is suggested to keep it open \nfor BiFrost. That way, you can choose different colors or patterns\nfor the secondary LED input of BiFrost.
Wire Notes Line
	650  4200 3550 4200
Wire Notes Line
	3550 4200 3550 5400
Wire Notes Line
	3550 5400 650  5400
Wire Notes Line
	650  5400 650  4200
Wire Notes Line
	650  5550 3550 5550
Wire Notes Line
	3550 5550 3550 6900
Wire Notes Line
	3550 6900 650  6900
Wire Notes Line
	650  5550 650  6900
Text Notes 4550 1350 0    50   ~ 0
The 5V input of J1 may be driven by „HDD+“ of the V1200\nfor typical operation with the factory stock LED module. \n\nIf you intend to operate BiFrost in extended mode, another \nsource for +5V is required. Please get the +5V directly from \nthe mainboard (e.g. 5V pin of the Floppy Connector or \nanother convenient place) in that case.
Text Notes 2000 2050 0    50   ~ 0
The output header J2 consists of two parts: \nPins 1-3 are only relevant for the BiFrost Heimdall ed. and map to XVCC, CH1 and CH2.\nPins 4-8 are a duplicate of the regular A1200 LED header. Don’t mount Pin7 for the regular A1200 LED module (key pin).
$Comp
L Device:R R5
U 1 1 6036B55F
P 5300 3700
F 0 "R5" H 5230 3654 50  0000 R CNN
F 1 "10k" H 5230 3745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5230 3700 50  0001 C CNN
F 3 "~" H 5300 3700 50  0001 C CNN
	1    5300 3700
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0111
U 1 1 6036C0A2
P 5300 3550
F 0 "#PWR0111" H 5300 3400 50  0001 C CNN
F 1 "+5V" H 5315 3723 50  0000 C CNN
F 2 "" H 5300 3550 50  0001 C CNN
F 3 "" H 5300 3550 50  0001 C CNN
	1    5300 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 6036D1A5
P 5300 5150
F 0 "R6" H 5230 5104 50  0000 R CNN
F 1 "10k" H 5230 5195 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5230 5150 50  0001 C CNN
F 3 "~" H 5300 5150 50  0001 C CNN
	1    5300 5150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 6036D5EF
P 5300 5000
F 0 "#PWR0112" H 5300 4850 50  0001 C CNN
F 1 "+5V" H 5315 5173 50  0000 C CNN
F 2 "" H 5300 5000 50  0001 C CNN
F 3 "" H 5300 5000 50  0001 C CNN
	1    5300 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3850 5000 3850
Wire Wire Line
	5000 3850 5000 3800
Connection ~ 5000 3800
Wire Wire Line
	5300 5300 5000 5300
Wire Wire Line
	5000 5300 5000 5150
Connection ~ 5000 5150
$Comp
L Device:R R8
U 1 1 6059D832
P 4150 5000
F 0 "R8" H 4080 4954 50  0000 R CNN
F 1 "15k" H 4080 5045 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4080 5000 50  0001 C CNN
F 3 "~" H 4150 5000 50  0001 C CNN
	1    4150 5000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 605A06F4
P 4100 3350
F 0 "R7" H 4030 3304 50  0000 R CNN
F 1 "15k" H 4030 3395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4030 3350 50  0001 C CNN
F 3 "~" H 4100 3350 50  0001 C CNN
	1    4100 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 5150 4150 5150
Connection ~ 4150 5150
Wire Wire Line
	4150 5150 4400 5150
$Comp
L power:+5V #PWR0113
U 1 1 605A2BCA
P 4150 4850
F 0 "#PWR0113" H 4150 4700 50  0001 C CNN
F 1 "+5V" H 4165 5023 50  0000 C CNN
F 2 "" H 4150 4850 50  0001 C CNN
F 3 "" H 4150 4850 50  0001 C CNN
	1    4150 4850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0114
U 1 1 605A3170
P 4100 3200
F 0 "#PWR0114" H 4100 3050 50  0001 C CNN
F 1 "+5V" H 4115 3373 50  0000 C CNN
F 2 "" H 4100 3200 50  0001 C CNN
F 3 "" H 4100 3200 50  0001 C CNN
	1    4100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3500 4100 3500
Connection ~ 4100 3500
Wire Wire Line
	4100 3500 4400 3500
Text Label 7950 3900 2    50   ~ 0
HDDLED_IN
$Comp
L Jumper:SolderJumper_2_Open Ori_LED2
U 1 1 612A5CEA
P 8150 3900
F 0 "Ori_LED2" H 8150 4105 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8150 4014 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8150 3900 50  0001 C CNN
F 3 "~" H 8150 3900 50  0001 C CNN
	1    8150 3900
	1    0    0    -1  
$EndComp
Text Label 8850 3900 2    50   ~ 0
HDDLED
Wire Wire Line
	8000 3900 7900 3900
Wire Wire Line
	8300 3900 8900 3900
$Comp
L Jumper:SolderJumper_2_Open Ori1
U 1 1 612A9DFB
P 8150 4250
F 0 "Ori1" H 8150 4455 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8150 4364 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8150 4250 50  0001 C CNN
F 3 "~" H 8150 4250 50  0001 C CNN
	1    8150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4250 7900 4250
Wire Wire Line
	7900 4250 7900 3900
Connection ~ 7900 3900
Wire Wire Line
	7900 3900 7550 3900
Text GLabel 8600 4250 2    50   Input ~ 0
inCH2
Wire Wire Line
	8300 4250 8600 4250
Text GLabel 7750 4800 0    50   Input ~ 0
CH1
Text GLabel 8700 4800 2    50   Input ~ 0
CH2
$Comp
L Jumper:SolderJumper_2_Open Ori2
U 1 1 612AD638
P 8150 4800
F 0 "Ori2" H 8150 5005 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8150 4914 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8150 4800 50  0001 C CNN
F 3 "~" H 8150 4800 50  0001 C CNN
	1    8150 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4800 7750 4800
Wire Wire Line
	8300 4800 8700 4800
Text Notes 6950 3300 0    50   ~ 0
Normally, this module can deal with 3.3V levels on the secondary\ninputs. These SJs allow to fix problems with the IDE device on\nthe motherboard connector. If you have a 5V compatible device\non motherboard IDE, just clode Ori_LED2.\nIf the motherboard IDE device is not 5V compatible, leave Ori_LED2\nopen and close Ori1 and Ori2. You lose the secondary input (inCH2)\nbut fix the onboard IDE lighting this way.
Wire Notes Line
	9650 2650 9650 6550
Wire Notes Line
	3650 2650 9650 2650
$EndSCHEMATC
